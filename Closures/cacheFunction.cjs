function cacheFunction(cb) {
    const cache = {};
    return function (elements) {
        const string = JSON.stringify(elements);
        //checking if elements array is  not empty 
        if (string.length > 0 && elements.length > 0) {
            if (!(string in cache)) {
                cache[string] = cb(elements);
                return cache[string];
            } else {
                return cache[string];
            }
        } else {
            return "Mention Input";
        }
    }
}

module.exports = cacheFunction;