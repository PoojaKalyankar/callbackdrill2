function limitFunctionCallCount(cb, n) {
  let count = 0;
  //error handling to check cb is function or not and n is number or not
  if (typeof cb !== 'function' || typeof n !== 'number') {
    return "cb is not a function (or) n is not a number ";
  }
  return function () {
    if (count < n) {
      count++;
      return cb();
    } else {
      return null;
    }
  }
}

module.exports = limitFunctionCallCount;