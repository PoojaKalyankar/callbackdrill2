function sortCarModels(inventory){
    let carModelsort=[];
    for(let index=0;index<inventory.length;index++){
        carModelsort.push(inventory[index].car_model);
    }
    carModelsort.sort();
    return carModelsort;
}
module.exports=sortCarModels;