function getListOfCards(cards, listId, callback) {
    try {
        setTimeout(() => {
            let answer = Object.entries(cards).filter(cardId => cardId[0] == listId);
            callback(null, Object.fromEntries(answer));
        }, 2000);
    } catch (error) {
        console.errorr(error);
    }
}

module.exports = getListOfCards;